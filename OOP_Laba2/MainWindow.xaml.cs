﻿using System;
using System.Windows;

namespace WpfCalculator
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CalculateButtonClick(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(XTextBox.Text, out double x) &&
                double.TryParse(YTextBox.Text, out double y) &&
                double.TryParse(ZTextBox.Text, out double z))
            {
            
                double s = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)) / (Math.Exp(x) * Math.Abs(Math.Cos(x + y))) + Math.Pow(Math.E, Math.Abs(x - y)) * (Math.Pow(Math.Tan(z), 2) + 1);

                ResultTextBlock.Text = $"Результат обчислення s = {s:F3}"; 
            }
            else
            {
         
                buttonClick();
            }
        }
      static  void buttonClick()
        {
        
            MessageBox.Show("Некоректний вивід!");
        }
    }
}
