﻿using System;
using System.Windows;

namespace WpfQuadraticSolver
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(ATextBox.Text, out double a) &&
                double.TryParse(BTextBox.Text, out double b) &&
                double.TryParse(CTextBox.Text, out double c))
            {
                double discriminant = b * b - 4 * a * c;

                if (discriminant > 0)
                {
                    double root1 = (-b + Math.Sqrt(discriminant)) / (2 * a);
                    double root2 = (-b - Math.Sqrt(discriminant)) / (2 * a);
                    ResultTextBlock.Text = $"Два корені: x1 = {root1:F3}, x2 = {root2:F3}";
                }
                else if (discriminant == 0)
                {
                    double root = -b / (2 * a);
                    ResultTextBlock.Text = $"Один корінь: x = {root:F3}";
                }
                else
                {
                    ResultTextBlock.Text = "Рівняння не має коренів у множині дійсних чисел.";
                }
            }
            else
            {
                ResultTextBlock.Text = "Помилка: Некоректне введення для a, b або c.";
            }
        }
    }
}
